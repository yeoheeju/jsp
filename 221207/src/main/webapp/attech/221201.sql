CREATE TABLE sample_tbl(
     sam1 VARCHAR2(20 byte),
     sam2 NUMBER(9, 2),
     sam3 DATE
);

SELECT * FROM sample_tbl;

SELECT *
FROM all_tables;  -- 모든 정보 다 보고싶으면 all

DROP TABLE sample_tbl;  -- DROP : 객체를 삭제 / DELETE : 정보를 삭제

CREATE TABLE emp_dept30( emp_id, f_name, dept)  -- SELECT에 별칭을 넣어도 되고 테이블이름 뒤에 적어도 가능
AS SELECT employee_id, first_name, department_id
   FROM employees
   WHERE department_id = 30;
   
SELECT * FROM emp_dept30;
DROP TABLE emp_dept30;

ALTER TABLE emp_dept30
ADD (hire_date DATE DEFAULT sysdate);    -- 넣을 컬럼을 나열하면 됨

ALTER TABLE emp_dept30
ADD (manager_id NUMBER(5, 0), sal NUMBER(9, 0));

ALTER TABLE emp_dept30
MODIFY manager_id VARCHAR2(10);

DESCRIBE emp_dept30;
INSERT INTO emp_dept30 VALUES(123, 'Dell', 50, TO_DATE('2022-12-01', 'yyyy-MM-dd'), 'Alex', 5233);
COMMIT;
SELECT * FROM emp_dept30; 

ALTER TABLE emp_dept30
MODIFY (manager_id VARCHAR2(12), SAL NUMBER(5, 0)); 

ALTER TABLE emp_dept30
DROP COLUMN manager_id; 

ALTER TABLE emp_dept30
SET UNUSED (sal);

SELECT * FROM emp_dept30; 

SELECT * FROM user_unused_col_tabs;
ALTER TABLE emp_dept30
DROP UNUSED COLUMNS;

DROP TABLE emp_dept30;  -- 테이블 삭제
-- PURGE 붙으면 복구못함 바로 소각장행

CREATE TABLE parent_tbl(
    p_id NUMBER(5),
    p_name VARCHAR2(10) NOT NULL,
    p_birth DATE,
    p_gender CHAR(1),
    --CONSTRAINT parnet_p_birth_uk UNIQUE (p_id, p_birth)
    CONSTRAINT parnet_p_id_uk UNIQUE (p_id),
    CONSTRAINT parnet_p_birth_uk UNIQUE (p_birth)
);

INSERT INTO parent_tbl(p_id) VALUES(1000);
INSERT INTO parent_tbl(p_name) VALUES('김나나'); --유니크는 널값을 허용

SELECT * FROM parent_tbl;

DROP TABLE parent_tbl;

INSERT INTO parent_tbl(p_id, p_name, p_birth) VALUES(1000, '김나나', '2022-12-01');
INSERT INTO parent_tbl(p_id, p_name, p_birth) VALUES(2000, '김나나', '2022-12-01');
INSERT INTO parent_tbl(p_id, p_name, p_birth) VALUES(1000, '김나나', '2022-12-02');

CREATE TABLE parent_tbl(
    p_id NUMBER(5, 0),
    p_name VARCHAR2(10),
    p_gender CHAR(1),
    CONSTRAINT parent_p_id_pk PRIMARY KEY(p_id, p_name) -- primary key 2개가 들어가면 오류남
);

INSERT INTO parent_tbl(p_id, p_name) VALUES(1000, '한난'); --pk이에 2개 들어갔으니까 여기도 2개 다 넣어줘야함
INSERT INTO parent_tbl(p_id, p_name) VALUES(1000, '김한');
INSERT INTO parent_tbl(p_id, p_name) VALUES(2000, '한난');
SELECT * FROM parent_tbl;

CREATE TABLE child_tbl(
       c_id NUMBER(5),
       c_phone VARCHAR2(15) DEFAULT 'OOO' NOT NULL UNIQUE,
       p_id NUMBER(7, 2), -- NUMBER(5, 0)
       p_name VARCHAR2(8), -- VARCHAR2(10)
       CONSTRAINT child_p_id_p_name_fk FOREIGN KEY(p_id, p_name)
          REFERENCES parent_tbl(p_id, p_name)
);

INSERT INTO child_tbl (c_id, p_id, p_name) VALUES(5000, 1000, '김한'); -- 이름달라서 오류떴었음
SELECT * FROM child_tbl;

DROP TABLE child_tbl;
CREATE TABLE child_tbl(
       c_id NUMBER(5) PRIMARY KEY, -- 1000 / 5->4자리 / 남는건 괜찮은데 넘기는건X
       c_gender CHAR(1),
       c_mgr NUMBER(5),
       CONSTRAINT child_c_mgr_fk FOREIGN KEY(c_mgr)
          REFERENCES child_tbl(c_id) ON DELETE CASCADE, -- 관련된거 다 삭제됨 / 없으면 잡고있는거 /
       CONSTRAINT child_c_gender_ck CHECK ( UPPER(c_gender) IN ('H', 'F')) -- LIKE, IN 쓰면 걍 체크 따라씀
);

INSERT INTO child_tbl (c_id, c_gender) VALUES(1000, 'H'); 
INSERT INTO child_tbl (c_id, c_gender) VALUES(2000, 'F');
INSERT INTO child_tbl VALUES(3000, 'H', 1000);
INSERT INTO child_tbl VALUES(4000, 'F', 1000);

DELETE FROM child_tbl WHERE c_id = 1000;
SELECT * FROM child_tbl;

DROP TABLE test_tbl;
CREATE TABLE test_tbl(
       t_id NUMBER(5),
       CONSTRAINT test_t_id_fk FOREIGN KEY (t_id) -- 외래키 fk 넣은거임
       REFERENCES child_tbl(c_id) 
);

INSERT INTO test_tbl VALUES (1000);
SELECT * FROM test_tbl;

DELETE FROM child_tbl;
ROLLBACK;

ALTER TABlE parent_tbl -- 추가
ADD CONSTRAINT parent_p_gender_ck CHECK ( UPPER(p_gender) IN ('M', 'F'));

ALTER TABLE parent_tbl
MODIFY p_gender CHAR(1) NOT NULL;

ALTER TABLE parent_tbl
DROP PRIMARY KEY; 

ALTER TABLE parent_tbl
DROP UNIQUE(p_id);

ALTER TABLE parent_tbl
DROP CONSTRAINT parent_p_gender_ck;

ALTER TABLE child_tbl
DISABLE PRIMARY KEY CASCADE; -- 비활성화


INSERT INTO test_tbl VAL'
UES(12345); 
INSERT INTO child_tbl(c_id) VALUES (null);

SELECT * FROM child_tbl;

ALTER TABLE child_tbl
ENABLE PRIMARY KEY; -- 활성화

ALTER TABLE child_tbl
ENABLE CONSTRAINT CHILD_C_MGR_FK;

--1)
CREATE TABLE y_dept(
       id NUMBER(7) PRIMARY KEY,
       name VARCHAR2(25));

ALTER TABlE y_dept
ADD (ADDR VARCHAR2(30)); -- ADD COLUMN addr VARCHAR2(30)

ALTER TABlE y_dept
MODIFY name VARCHAR2(25) NOT NULL;
DESCRIBE y_dept;

--2)
CREATE TABLE y_emp(
       id NUMBER(7, 0) PRIMARY KEY,
       last_name VARCHAR2(25),
       first_name  VARCHAR2(25),
       dept_id NUMBER(7, 0),
       CONSTRAINT y_emp_dept_id_fk FOREIGN KEY (dept_id) 
          REFERENCES y_dept(id)
);
DESCRIBE y_emp; -- 콘솔창에 바로 확인

------------ VIEW-------------
CREATE OR REPLACE VIEW v_emp10 (empId, ename, hire_d, sal,dept) --or replace를 쓰면 뷰가 기존에 있는건지 새로 쓴건지 확인할수 없어서 조심해야함
AS 
  SELECT employee_id, first_name, hire_date, salary, department_id
  FROM employees
  WHERE department_id = 10
   WITH CHECK OPTION;
  
SELECT * FROM v_emp10;

-- 위랑 같음
SELECT * FROM(SELECT employee_id, first_name, hire_date, salary
              FROM employees
              WHERE department_id = 10);
              
DROP VIEW v_emp10;

INSERT INTO v_emp10 VALUES(1234, 'Hong', TO_DATE('2022-12-01', 'yyyy-MM-dd'), 10000);
UPDATE v_emp10
   SET ename = 'Hong';
   
SELECT * FROM v_emp10;
SELECT * FROM employees;

DELETE v_emp10; -- 오류남

UPDATE v_emp10
  SET ename = 'Shin'
  WHERE empid = 200;
  
ROLLBACK;

---------INDEX----------
CREATE INDEX employees_sal_idx
ON employees(salary); --on을 붙이면서 어느 테이블에 어느 컬럼을 붙일건지 정하는거

DROP INDEX employees_sal_idx; -- 내가 만들지 않은 인덱스는 삭제못함