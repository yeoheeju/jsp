package co.micol.prj.notice.command;

import java.io.IOException;
import java.sql.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.oreilly.servlet.MultipartRequest;
import com.oreilly.servlet.multipart.DefaultFileRenamePolicy;

import co.micol.prj.common.Command;
import co.micol.prj.notice.service.NoticeService;
import co.micol.prj.notice.service.NoticeVO;
import co.micol.prj.notice.serviceImpl.NoticeServiceImpl;

public class NoticeInsert implements Command {

	@Override
	public String exec(HttpServletRequest request, HttpServletResponse reponse) {
		// 공지사항 글등록 하기 MutiPartRequest를 이용해야한다.(cos.jar)
		NoticeService dao = new NoticeServiceImpl();
		NoticeVO vo = new NoticeVO();
		
		String saveDir = request.getServletContext().getRealPath("/attech/");
		
		int maxSize = 1024*1024*1024; // 최대100MB까지 업로드
		try {
			// 파일을 업로드시 request객체를 대체한다.
			MultipartRequest multi = new MultipartRequest(request, saveDir, maxSize, "utf-8",
					new DefaultFileRenamePolicy());
		
			vo.setNoticeWriter(multi.getParameter("noticeWriter"));
			vo.setNoticeDate(Date.valueOf(multi.getParameter("noticeDate")));
			vo.setNoticeTitle(multi.getParameter("noticeTitle"));
			vo.setNoticeSubject(multi.getParameter("noticeSubject"));
			
			
			String ofileName = multi.getOriginalFileName("nfile"); // 원본 파일이름
			String pfileName = multi.getFilesystemName("nfile");// 저장되는 파일이름
			
			if(ofileName != "") {
				vo.setNoticeFile(pfileName);
				pfileName = saveDir + pfileName; // 저장directory
				vo.setNoticeFileDir(pfileName);
			}
			
			int n = dao.noticeInsert(vo);
			if(n != 0) {
				request.setAttribute("message", "공지사항이 등록되었습니다.");
			} else {
				request.setAttribute("message", "공지사항 등록에 실패했습니다.");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "notice/noticeMessage.tiles";
	}
}
